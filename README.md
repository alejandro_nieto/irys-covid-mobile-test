# Covid Mobile Test Solution by Alejandro Nieto

Here I solve the test sent, I named the views as these:

  - CategoryList -> Categories
  - SubCategoriesList -> Report
  - Description view -> AddReport
  - Map view -> Map
  - UserForm -> I added this one
 
I based my solution on the website provided in this link https://covid19.heyirys.com/app/

I used flux architecture

I created a fakeApi.js file to simulate the api request to the server and modified the json provided on order to achieve a better user experience.

The app accomplish a complete flow from select a category, open a link, create an incidence, save the location and a form to submit the user information