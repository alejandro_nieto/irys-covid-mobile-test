// @flow
import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import I18n from 'app/i18n/i18n';
import Categories from '../containers/Categories/Categories';
import Report from '../containers/Report/Report';
import Resources from '../containers/Resources/Resources';
import AddReport from '../containers/AddReport/AddReport';
import Map from '../containers/Map/Map';
import UserForm from '../containers/UserForm/UserForm';
import {COLORS} from '../assets/theme-vars';

export default class Navigator extends React.Component<{}> {
  render() {
    return <AppContainer />;
  }
}
const headerOptions = {
  title: I18n.t('mainTitle'),
  headerStyle: {
    backgroundColor: COLORS.primary,
  },
  headerTintColor: COLORS.secondary,
  headerTitleStyle: {
    color: COLORS.secondary,
  },
};
const StackNavigator = createStackNavigator(
  {
    Categories: {
      screen: Categories,
      navigationOptions: headerOptions,
    },
    Report: {
      screen: Report,
      navigationOptions: headerOptions,
    },
    Resources: {
      screen: Resources,
      navigationOptions: headerOptions,
    },
    AddReport: {
      screen: AddReport,
      navigationOptions: headerOptions,
    },
    Map: {
      screen: Map,
      navigationOptions: headerOptions,
    },
    UserForm: {
      screen: UserForm,
      navigationOptions: headerOptions,
    },
  },
  {
    initialRouteName: 'Categories',
  },
);

const AppContainer = createAppContainer(StackNavigator);
