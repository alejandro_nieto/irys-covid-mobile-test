// @flow
import React, {useState} from 'react';

const AppContext: any = React.createContext({});

type Props = {
  children: React$Node,
};

const AppContextProvider = ({children}: Props) => {
  const [issue, setIssue] = useState('');
  const [location, setLocation] = useState('');
  const [user, setUser] = useState({
    name: '',
    phone: '',
    email: '',
  });
  return (
    <AppContext.Provider
      value={{
        issue,
        setIssue,
        user,
        setUser,
        location,
        setLocation,
      }}>
      {children}
    </AppContext.Provider>
  );
};

export {AppContext, AppContextProvider};
