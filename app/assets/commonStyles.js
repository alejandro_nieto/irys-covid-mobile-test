import {StyleSheet} from 'react-native';
import {COLORS} from './theme-vars';
import normalize from 'react-native-normalize';

const commonStyles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.primary,
    height: '100%',
  },
  scrollView: {
    paddingHorizontal: normalize(20),
    paddingVertical: normalize(60),
  },
  body: {
    marginBottom: normalize(40),
  },
});

export default commonStyles;
