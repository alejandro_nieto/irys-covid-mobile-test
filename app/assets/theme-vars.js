const COLORS = {
  primary: '#f5f0ff',
  secondary: '#1b112a',
  active: '#bab6c2',
  red: '#c51d31',
  title: '#4f4954',
  white: '#ffffff',
};

export {COLORS};
