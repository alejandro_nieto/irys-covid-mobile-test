/**
 * @format
 * @flow strict-local
 */

import React, {useState, useContext} from 'react';
import {SafeAreaView, Alert, View, StatusBar, Text} from 'react-native';
import I18n from 'app/i18n/i18n';
import {isEmpty} from 'lodash';
import commonStyles from 'app/assets/commonStyles';
import CustomHeader from '../../components/CustomHeader/CustomHeader';
import CustomButton from '../../components/CustomButton/CustomButton';
import CustomInput from '../../components/CustomInput/CustomInput';
import {AppContext} from 'app/config/AppContext/AppContext';
import styles from './styles';

const AddReport: () => React$Node = ({navigation}: any) => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const {location, issue, setUser} = useContext(AppContext);

  const submitForm = () => {
    setUser({
      name,
      email,
      phone,
    });
    Alert.alert(
      I18n.t('thanksTitle'),
      I18n.t('thanksMessage', {userName: name}),
    );
  };

  const isFormValid = !isEmpty(name) && !isEmpty(email) && !isEmpty(phone);

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={commonStyles.container}>
        <View style={styles.container}>
          <View style={commonStyles.body}>
            <CustomHeader
              title={I18n.t('userFormTitle')}
              subTitle={I18n.t('userFormDescription')}
            />
            <CustomInput
              placeholder={I18n.t('name')}
              value={name}
              onChangeValue={setName}
            />
            <CustomInput
              placeholder={I18n.t('email')}
              value={email}
              onChangeValue={setEmail}
            />
            <CustomInput
              placeholder={I18n.t('phone')}
              value={phone}
              onChangeValue={setPhone}
            />
          </View>
          {(!isEmpty(issue) || !isEmpty(location)) && (
            <View>
              <Text style={styles.title}>{I18n.t('currentInformation')}</Text>
              {!isEmpty(location) && (
                <Text style={styles.description} numberOfLines={1}>
                  {I18n.t('location')}: {location}
                </Text>
              )}
              {!isEmpty(issue) && (
                <Text style={styles.description} numberOfLines={1}>
                  {I18n.t('issue')}: {issue}
                </Text>
              )}
            </View>
          )}
          <View style={styles.buttonsContainer}>
            <CustomButton
              onPress={() => navigation.goBack()}
              inverse
              label={I18n.t('back')}
            />
            <CustomButton
              disabled={!isFormValid}
              onPress={submitForm}
              label={I18n.t('submit')}
            />
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default AddReport;
