import {StyleSheet} from 'react-native';
import {COLORS} from 'app/assets/theme-vars';
import normalize from 'react-native-normalize';

const commonStyles = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20),
    paddingVertical: normalize(60),
    justifyContent: 'space-between',
    height: '100%',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    color: COLORS.secondary,
    fontWeight: 'bold',
    fontSize: normalize(18),
  },
  description: {
    color: COLORS.secondary,
    fontSize: normalize(16),
  },
});

export default commonStyles;
