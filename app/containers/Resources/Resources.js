/**
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {SafeAreaView, ScrollView, View, StatusBar, Linking} from 'react-native';
import I18n from 'app/i18n/i18n';
import commonStyles from 'app/assets/commonStyles';
import {getLinksByOrder} from 'app/services/categories.service';
import CustomHeader from 'app/components/CustomHeader/CustomHeader';
import CustomList from 'app/components/CustomList/CustomList';

const Resources: () => React$Node = ({navigation}: any) => {
  const [links, setLinks] = useState([]);

  useEffect(() => {
    const init = async () => {
      const categoryOrder = navigation.getParam('categoryOrder');
      const _links = getLinksByOrder(categoryOrder);
      setLinks(
        _links.map(({url, title}) => ({
          key: url,
          name: title,
          url,
        })),
      );
    };
    init();
  }, [navigation]);

  const onPressItem = (item) => Linking.openURL(item.url);

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={commonStyles.container}>
        <ScrollView style={commonStyles.scrollView}>
          <View style={commonStyles.body}>
            <CustomHeader
              title={I18n.t('resources')}
              subTitle={I18n.t('resourcesDescription')}
            />

            <CustomList items={links} onPressItem={onPressItem} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Resources;
