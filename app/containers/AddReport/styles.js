import {StyleSheet} from 'react-native';
import {COLORS} from 'app/assets/theme-vars';
import normalize from 'react-native-normalize';

const commonStyles = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20),
    paddingVertical: normalize(60),
    justifyContent: 'space-between',
    height: '100%',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textarea: {
    width: '100%',
    borderColor: COLORS.secondary,
    borderWidth: 1,
    borderRadius: 20,
    height: normalize(200, 'height'),
    padding: normalize(20),
    marginVertical: normalize(20),
  },
  input: {
    fontSize: normalize(16),
    color: COLORS.normalize,
    flex: 1,
  },
  charsIndicator: {
    fontSize: normalize(14),
    color: COLORS.normalize,
    position: 'absolute',
    bottom: normalize(10),
    right: normalize(10),
  },
});

export default commonStyles;
