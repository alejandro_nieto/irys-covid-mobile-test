/**
 * @format
 * @flow strict-local
 */

import React, {useContext, useState} from 'react';
import {SafeAreaView, View, StatusBar, TextInput, Text} from 'react-native';
import {isEmpty} from 'lodash';
import I18n from 'app/i18n/i18n';
import commonStyles from 'app/assets/commonStyles';
import CustomHeader from '../../components/CustomHeader/CustomHeader';
import CustomButton from '../../components/CustomButton/CustomButton';
import {AppContext} from 'app/config/AppContext/AppContext';
import {COLORS} from '../../assets/theme-vars';
import styles from './styles';

const AddReport: () => React$Node = ({navigation}: any) => {
  const [issueDescription, setIssueDescription] = useState('');
  const {setIssue} = useContext(AppContext);

  const submitIssue = () => {
    setIssue(issueDescription);
    navigation.navigate('Map');
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={commonStyles.container}>
        <View style={styles.container}>
          <View>
            <CustomHeader
              title={I18n.t('reportTitle')}
              subTitle={I18n.t('briefDescription')}
            />
            <View style={styles.textarea}>
              <TextInput
                multiline={true}
                maxLength={200}
                numberOfLines={4}
                onChangeText={setIssueDescription}
                value={issueDescription}
                placeholder={I18n.t('reportPlaceHolder')}
                placeholderTextColor={COLORS.secondary}
                style={styles.input}
              />
              <Text style={styles.charsIndicator}>
                {issueDescription.length}/200
              </Text>
            </View>
          </View>
          <View style={styles.buttonsContainer}>
            <CustomButton
              onPress={() => navigation.goBack()}
              inverse
              label={I18n.t('back')}
            />
            <CustomButton
              disabled={isEmpty(issueDescription)}
              onPress={submitIssue}
              label={I18n.t('next')}
            />
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default AddReport;
