/**
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {getLanguages} from 'react-native-i18n';
import {get} from 'lodash';
import {SafeAreaView, ScrollView, View, StatusBar} from 'react-native';
import commonStyles from 'app/assets/commonStyles';
import I18n from 'app/i18n/i18n';
import {getSubCategoriesByOrder} from 'app/services/categories.service';
import CustomHeader from 'app/components/CustomHeader/CustomHeader';
import CustomList from 'app/components/CustomList/CustomList';

const Report: () => React$Node = ({navigation}: any) => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    const init = async () => {
      const categoryOrder = navigation.getParam('categoryOrder');
      const subCategories = getSubCategoriesByOrder(categoryOrder);
      const languages = await getLanguages();
      setItems(
        subCategories.map((category) => ({
          ...category,
          name: get(category.name, languages, category.name.en),
          key: category.type,
        })),
      );
    };
    init();
  }, [navigation]);

  const onPressItem = (category) => {
    navigation.navigate('AddReport', {
      category,
    });
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={commonStyles.container}>
        <ScrollView style={commonStyles.scrollView}>
          <View style={commonStyles.body}>
            <CustomHeader
              title={I18n.t('reportTitle')}
              subTitle={I18n.t('reportDescription')}
            />

            <CustomList items={items} onPressItem={onPressItem} />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Report;
