/**
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {SafeAreaView, View, StatusBar} from 'react-native';
import {getLanguages} from 'react-native-i18n';
import {get} from 'lodash';
import I18n from 'app/i18n/i18n';
import commonStyles from 'app/assets/commonStyles';
import CustomHeader from '../../components/CustomHeader/CustomHeader';
import CustomList from '../../components/CustomList/CustomList';
import {getCategories} from '../../services/categories.service';
import styles from './styles';

const Categories: () => React$Node = ({navigation}: any) => {
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    const init = async () => {
      const _categories = getCategories();
      const languages = await getLanguages();
      setCategories(
        _categories.map((category) => ({
          ...category,
          key: category.order,
          name: get(category.name, languages, category.name.en),
        })),
      );
    };
    init();
  }, []);

  const onPressItem = ({order: categoryOrder}) => {
    if (categoryOrder === '1') {
      return navigation.navigate('Map');
    }
    if (categoryOrder === '2') {
      return navigation.navigate('Report', {
        categoryOrder,
      });
    }
    return navigation.navigate('Resources', {
      categoryOrder,
    });
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={commonStyles.container}>
        <View style={styles.container}>
          <View>
            <CustomHeader
              title={I18n.t('category')}
              subTitle={I18n.t('selectAnOption')}
            />
            <CustomList items={categories} onPressItem={onPressItem} />
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Categories;
