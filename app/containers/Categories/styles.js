import {StyleSheet} from 'react-native';
import normalize from 'react-native-normalize';

const commonStyles = StyleSheet.create({
  container: {
    paddingHorizontal: normalize(20),
    paddingVertical: normalize(60),
  },
});

export default commonStyles;
