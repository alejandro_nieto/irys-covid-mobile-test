import {StyleSheet} from 'react-native';
import {COLORS} from 'app/assets/theme-vars';
import normalize from 'react-native-normalize';

const commonStyles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    height: '88%',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: normalize(20),
    bottom: normalize(70),
  },
  customInput: {
    alignSelf: 'center',
    width: '80%',
    borderColor: COLORS.secondary,
    borderWidth: 1,
    borderRadius: 20,
    position: 'absolute',
    backgroundColor: COLORS.primary,
    paddingVertical: normalize(13),
    paddingHorizontal: normalize(20),
    marginVertical: normalize(20),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  downArrow: {
    width: normalize(15),
    height: normalize(15),
  },
  mapStyles: {
    width: '100%',
    height: '100%',
  },
  bottomPlaceholder: {
    padding: normalize(20),
    color: COLORS.secondary,
    backgroundColor: COLORS.primary,
    height: 100,
    textAlign: 'center',
    fontSize: normalize(16),
  },
});

export default commonStyles;
