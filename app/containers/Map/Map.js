/**
 * @format
 * @flow strict-local
 */

import React, {useContext} from 'react';
import {
  SafeAreaView,
  View,
  StatusBar,
  TextInput,
  Text,
  Image,
} from 'react-native';
import I18n from 'app/i18n/i18n';
import MapView, {Marker} from 'react-native-maps';
import {isEmpty} from 'lodash';
import downArrow from 'app/assets/icons/downArrow.png';
import commonStyles from 'app/assets/commonStyles';
import {AppContext} from 'app/config/AppContext/AppContext';
import CustomButton from '../../components/CustomButton/CustomButton';
import styles from './styles';
import {COLORS} from '../../assets/theme-vars';

const AddReport: () => React$Node = ({navigation}: any) => {
  const {location, setLocation} = useContext(AppContext);

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={commonStyles.container}>
        <View style={styles.container}>
          <View>
            <MapView
              style={styles.mapStyles}
              initialRegion={{
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}>
              <Marker
                coordinate={{
                  latitude: 37.78825,
                  longitude: -122.4324,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                }}
              />
            </MapView>
            <View style={styles.customInput}>
              <TextInput
                maxLength={200}
                numberOfLines={1}
                onChangeText={setLocation}
                value={location}
                placeholder={I18n.t('locationPlaceHolder')}
                placeholderTextColor={COLORS.secondary}
                style={styles.input}
              />
              <Image source={downArrow} style={styles.downArrow} />
            </View>
            <View style={styles.buttonsContainer}>
              <CustomButton
                onPress={() => navigation.goBack()}
                inverse
                label={I18n.t('back')}
              />
              <CustomButton
                onPress={() => navigation.navigate('UserForm')}
                label={isEmpty(location) ? I18n.t('skip') : I18n.t('next')}
              />
            </View>
          </View>
          <Text style={styles.bottomPlaceholder}>
            {I18n.t('mapBottomPlaceHolder')}
          </Text>
        </View>
      </SafeAreaView>
    </>
  );
};

export default AddReport;
