export default {
  mainTitle: 'COVID-19 Support',
  category: 'Category',
  selectAnOption: 'Select an option to continue',
  back: 'Back',
  next: 'Next',
  reportTitle: 'Report Price Gouging',
  reportDescription:
    'Please select the type of the product or service that is being sold at an exorbitant or excessive price',
  resources: 'COVID-19 Resources',
  resourcesDescription: 'Here a list of useful information about COVID-19',
  briefDescription: 'Please provide a brief description of the issue',
  reportPlaceHolder: 'Type here, max 200 characters allowed.',
  mapBottomPlaceHolder:
    'Please provide your approximate location. This information will be kept confidential',
  locationPlaceHolder: 'Type your location',
  userFormTitle: 'Personal Information',
  userFormDescription: 'Please provide contact information',
  name: 'Name',
  email: 'Email',
  phone: 'Phone',
  submit: 'Submit',
  skip: 'Skip',
  location: 'Location',
  issue: 'Issue',
  currentInformation: 'Current information',
  thanksTitle: 'Data saved',
  thanksMessage: 'Thanks {{userName}} for trust in us',
};
