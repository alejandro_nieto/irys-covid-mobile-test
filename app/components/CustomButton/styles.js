import {StyleSheet} from 'react-native';
import {COLORS} from '../../assets/theme-vars';
import normalize from 'react-native-normalize';

const styles = StyleSheet.create({
  customButton: {
    backgroundColor: COLORS.red,
    paddingVertical: 20,
    alignItems: 'center',
    borderColor: COLORS.red,
    borderWidth: 1,
    borderRadius: 10,
    minWidth: normalize(160),
  },
  label: {
    textAlign: 'center',
    color: COLORS.white,
    fontSize: normalize(14),
  },
  disabled: {
    opacity: 0.7,
  },
  inverse: {
    backgroundColor: COLORS.primary,
  },
});

export default styles;
