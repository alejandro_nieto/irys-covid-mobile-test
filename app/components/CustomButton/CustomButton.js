/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styles from './styles';
import {COLORS} from '../../assets/theme-vars';

type CustomButtonProps = {
  label: string,
  onPress: string,
  inverse: boolean,
  disabled: boolean,
};

const CustomButton: () => React$Node = ({
  label,
  inverse,
  onPress,
  disabled,
}: CustomButtonProps) => (
  <TouchableOpacity
    disabled={disabled}
    onPress={onPress}
    style={[
      styles.customButton,
      disabled ? styles.disabled : {},
      inverse ? styles.inverse : {},
    ]}>
    <Text
      style={[
        styles.label,
        {
          color: inverse ? COLORS.red : COLORS.white,
        },
      ]}>
      {label}
    </Text>
  </TouchableOpacity>
);

export default CustomButton;
