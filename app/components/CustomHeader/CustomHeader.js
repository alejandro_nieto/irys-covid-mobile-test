/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import {Text} from 'react-native';
import styles from './styles';

type HeaderProps = {
  title: string,
  subTitle: string,
};

const CustomHeader: () => React$Node = ({title, subTitle}: HeaderProps) => (
  <>
    <Text style={styles.title}>{title}</Text>
    <Text style={styles.subtitle}>{subTitle}</Text>
  </>
);

export default CustomHeader;
