import {StyleSheet} from 'react-native';
import {COLORS} from '../../assets/theme-vars';
import normalize from 'react-native-normalize';

const styles = StyleSheet.create({
  title: {
    color: COLORS.secondary,
    fontWeight: 'bold',
    fontSize: normalize(18),
  },
  subtitle: {
    color: COLORS.secondary,
    fontSize: normalize(14),
    marginVertical: normalize(20),
  },
});

export default styles;
