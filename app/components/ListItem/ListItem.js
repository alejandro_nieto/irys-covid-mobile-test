/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import arrow from '../../assets/icons/arrow.png';
import styles from './styles';

type ListItemProps = {
  icon: any,
  text: string,
  onPressItem: () => void,
};

const ListItem: () => React$Node = ({
  icon,
  text,
  onPressItem,
}: ListItemProps) => {
  return (
    <TouchableOpacity onPress={onPressItem} style={styles.listItem}>
      <View style={styles.textContainer}>
        {icon && <Image style={styles.icon} source={{uri: icon}} />}
        <Text style={styles.text}>{text}</Text>
      </View>
      <Image source={arrow} style={styles.arrow} />
    </TouchableOpacity>
  );
};

export default ListItem;
