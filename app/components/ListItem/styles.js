import {StyleSheet} from 'react-native';
import {COLORS} from '../../assets/theme-vars';
import normalize from 'react-native-normalize';

const styles = StyleSheet.create({
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: normalize(38),
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: COLORS.secondary,
  },
  active: {
    backgroundColor: COLORS.active,
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: {
    color: COLORS.secondary,
    fontWeight: 'bold',
    fontSize: normalize(16),
  },
  icon: {
    width: normalize(38),
    height: normalize(38),
    marginRight: normalize(20),
  },
  arrow: {
    width: normalize(20),
    height: normalize(20),
  },
});

export default styles;
