/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import ListItem from '../../components/ListItem/ListItem';

type CustomListProps = {
  items: any,
  onPressItem: (item: any) => void,
};

const CustomList: () => React$Node = ({
  items,
  onPressItem,
}: CustomListProps) => {
  return (
    <>
      {items.map((item) => (
        <ListItem
          key={item.key}
          text={item.name}
          icon={item.icon}
          onPressItem={() => onPressItem(item)}
        />
      ))}
    </>
  );
};

export default CustomList;
