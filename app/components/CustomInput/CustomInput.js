/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View, TextInput} from 'react-native';
import styles from './styles';
import {COLORS} from '../../assets/theme-vars';

type CustomInputProps = {
  value: string,
  placeholder: string,
  onChangeValue: (value: string) => void,
};

const CustomInput: () => React$Node = ({
  value,
  placeholder,
  onChangeValue,
}: CustomInputProps) => {
  return (
    <View style={styles.customInput}>
      <TextInput
        maxLength={200}
        numberOfLines={1}
        onChangeText={onChangeValue}
        value={value}
        placeholder={placeholder}
        placeholderTextColor={COLORS.secondary}
      />
    </View>
  );
};

export default CustomInput;
