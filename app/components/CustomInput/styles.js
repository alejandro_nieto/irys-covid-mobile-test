import {StyleSheet} from 'react-native';
import {COLORS} from 'app/assets/theme-vars';
import normalize from 'react-native-normalize';

const commonStyles = StyleSheet.create({
  customInput: {
    width: '90%',
    borderColor: COLORS.secondary,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: COLORS.primary,
    paddingVertical: normalize(13),
    paddingHorizontal: normalize(20),
    marginVertical: normalize(10),
  },
});

export default commonStyles;
