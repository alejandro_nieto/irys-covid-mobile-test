import DATA from './data.json';

export const getCategories = () => {
  const categories = Object.keys(DATA.categories).map(
    (key) => DATA.categories[key],
  );

  return categories.sort((a, b) => {
    if (a.order < b.order) {
      return -1;
    }
    if (a.order > b.order) {
      return 1;
    }
  });
};

// I use order as an Id, a proper request should always use a valid id
export const getSubCategories = (categoryOrder) => {
  const categories = Object.keys(DATA.categories).map((key) => ({
    ...DATA.categories[key],
    type: key,
  }));

  const category =
    categories.filter(({order}) => order === categoryOrder)[0] || {};

  let subCategories = category.subcategories || [];

  subCategories = Object.keys(subCategories).map((key) => ({
    ...subCategories[key],
    type: key,
    parentCategory: category.type,
  }));

  return subCategories || [];
};

// I use order as an Id, a proper request should always use a valid id
export const getLinks = (categoryOrder) => {
  const categories = Object.keys(DATA.categories).map(
    (key) => DATA.categories[key],
  );

  let links =
    (categories.filter((category) => category.order === categoryOrder)[0] || {})
      .links || [];

  links = Object.keys(links).map((key) => links[key]);

  return links || [];
};
