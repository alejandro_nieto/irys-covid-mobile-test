// @flow

import React from 'react';
import Navigator from './config/Navigator';
import {AppContextProvider} from './config/AppContext/AppContext';

export default class App extends React.Component<{}> {
  render() {
    return (
      <AppContextProvider>
        <Navigator />
      </AppContextProvider>
    );
  }
}
