import {
  getCategories as _getCategories,
  getSubCategories,
  getLinks,
} from '../fakeApi';

export const getCategories = () => {
  return _getCategories();
};

export const getSubCategoriesByOrder = (categoryOrder) => {
  return getSubCategories(categoryOrder);
};

export const getLinksByOrder = (categoryOrder) => {
  return getLinks(categoryOrder);
};
